#! /usr/bin/env bash -e

# -- SET ENV VARS -- #
# bucket the webhook and jar file are uploaded to
BUCKET_NAME=

# secret to authenticate and (implicitly) authorize webhook update request
WEBHOOK_AUTH_SECRET=

# path to the jar file in the bucket
# ex: /app.jar (if file is in the root bucket directory)
BUCKET_JAR_PATH=/app.jar
# -- END ENV VARS -- #

# write the bucket name and jar path to the webhook/setup.sh script
sed -i.tmp -E '1,/BUCKET_NAME/ s~(BUCKET_NAME=)(.*)~\1'"${BUCKET_NAME}"'~; 1,/BUCKET_JAR_PATH/ s~(BUCKET_JAR_PATH=)(.*)~\1'"${BUCKET_JAR_PATH}"'~; 1,/WEBHOOK_AUTH_SECRET/ s~(WEBHOOK_AUTH_SECRET=)(.*)~\1'"${WEBHOOK_AUTH_SECRET}"'~' webhook/setup.sh && rm webhook/setup.sh.tmp

# upload to the bucket
aws s3 cp --recursive webhook "s3://${BUCKET_NAME}/webhook"

echo "SSH into your instance and run the following command"

echo "aws s3 cp --recursive s3://${BUCKET_NAME}/webhook /etc/opt/zika/webhook"
