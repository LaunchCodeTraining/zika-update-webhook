#! /usr/bin/env bash

# -e: exit if any step fails, webhook server listens for exit code
# -x: print each command as executed to view in logs with $ journalctl -fu webhook
set -ex

# pull new app.jar
aws s3 cp "s3://${BUCKET_NAME}${BUCKET_JAR_PATH}" /tmp/app.jar

systemctl stop zika

mv /tmp/app.jar "$JAR_TARGET_PATH"
chown zika:zika "$JAR_TARGET_PATH"
chmod +x "$JAR_TARGET_PATH"

systemctl start zika

# clean exit for webhook response
exit 0