#! /usr/bin/env bash

# exit on failure
set -e


# -- SET ENV VARS -- #
# where to store the webhook service files
webhook_target_dir=/etc/opt/zika/webhook

# the bucket to download the jar file from
# ex: launchcode-gis-c6-patrick-zika
BUCKET_NAME=

# path to the jar file in the bucket
# ex: /app.jar (if file is in the root bucket directory)
BUCKET_JAR_PATH=

# secret to authenticate and (implicitly) authorize webhook update request
WEBHOOK_AUTH_SECRET=

# absolute path of where to send the new jar file
JAR_TARGET_PATH=/opt/zika/app.jar
# -- END ENV VARS -- #

# make webhook script executable
chown -R zika:zika "$webhook_target_dir"
chmod +x "${webhook_target_dir}/server.py"

# move service file
mv "${webhook_target_dir}/webhook.service" /etc/systemd/system/

cat << EOF > "${webhook_target_dir}/webhook.config"
BUCKET_NAME=$BUCKET_NAME
BUCKET_JAR_PATH=$BUCKET_JAR_PATH
JAR_TARGET_PATH=$JAR_TARGET_PATH
WEBHOOK_AUTH_SECRET=$WEBHOOK_AUTH_SECRET
EOF

# enable and start (--now) the webhook service
systemctl enable --now webhook
