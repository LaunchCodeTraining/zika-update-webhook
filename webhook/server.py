#! /usr/bin/env python

# support for python3 
def get_python3_imports():
  from http.server import HTTPServer, BaseHTTPRequestHandler

  return [HTTPServer, BaseHTTPRequestHandler]

# support for python 2
def get_python2_imports():
  from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

  return [HTTPServer, BaseHTTPRequestHandler]

# get the path to the update-api.sh script
def get_script_path(command_line_args, env_vars):
  # script path can be set as an arg to calling the program
  if len(command_line_args) > 1:
    return command_line_args[1]
  
  # otherwise check for an environment variable value
  # if all else fails use the default location
  return env_vars.get("UPDATE_API_SCRIPT_PATH", "/etc/opt/zika/webhook/update-api.sh")

def create_server(imports, script_path, auth_secret):
  from subprocess import call

  [HTTPServer, BaseHTTPRequestHandler] = imports

  class WebhookHandler(BaseHTTPRequestHandler):
    def send_status(self, status_code):
      self.send_response(status_code)
      self.end_headers()

    def is_authed(self):
      auth_header = self.headers.get("Authorization", None)
      
      return auth_header == auth_secret if auth_header else False

      
    # this behaves like a router, checking paths and delegating to route handler methods
    def do_GET(self):
      # only requests to /update (or whatever you set here) trigger the update
      if self.path == "/update":
        if self.is_authed():
          self.call_update_api(script_path)
        else:
          self.send_status(403)
      else:
        self.send_status(404)
    
    def call_update_api(self, update_api_script_path):
      # default status code and message unless changed below
      status_code = 200

      try:
        exit_code = call(["bash", update_api_script_path])
        
        if exit_code != 0: status_code = 500
      
      except OSError: status_code = 500

      # send the final status code
      self.send_status(status_code)

  return HTTPServer(("localhost", 8008), WebhookHandler)

def run():
  from os import environ
  from sys import argv, version_info

  # path to update-api bash script
  script_path = get_script_path(argv, environ)
  auth_secret = environ.get("WEBHOOK_AUTH_SECRET", None)
  
  has_python3 = version_info[0] == 3 # python version
  # same imports but different sources depending on python version
  imports = get_python3_imports() if has_python3 else get_python2_imports()

  server = create_server(imports, script_path, auth_secret)

  while True:
    server.handle_request()

# when a python script is imported its __name__ is the filename
# when it is executed directly its __name__ is "__main__"
if __name__ == "__main__":
  # if this script is executed then call the run() method
  run()