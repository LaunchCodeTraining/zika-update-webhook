# Update API Webhook

A webhook server is a small server that listens for requests on a **webhook endpoint**. When it receives a request it triggers some action to take place (like running a script). In this case the action is to deploy a new version of the API.

It will download a new `app.jar` file from the bucket it was delivered to by your pipeline, replace the existing file, and restart the Zika API service.

## NOTE

In ths basic version of this webhook you saw the following warning:

> This endpoint **is not secure and is only a proof of concept**! Anyone can issue a request to `:8008/update` on your EC2 instance to trigger an update.

For additional security this version requires the client that requests the update (you, a pipeline) to **authenticate** itself. Authentication, in the form of a secret, is sent in an `Authorization` header. This will be a **Knowledge-factor** authentication with _implicit_ authorization. Meaning if the requestor has the secret then they are authorized to call for an update to the API.

> **Make sure not to commit the `deliver.sh` or `webhook/setup.sh` files as they include this secret value!**

## AWS Setup

> **If your EC2 instance does not have a service role attached to it you need to create one**.

Recall that a service role is an IAM role that gives the EC2 instance it is assigned to access privileges to AWS services on your behalf. It will be given any privileges for resources defined in the role's attached policies. For this webhook to work you need at minimum a policy granting access to the S3 bucket. Instructions on setting up the service role are at the end of this file.

# Usage

1. set your bucket name, `app.jar` object path (in the bucket) and webhook auth secret in the `deliver.sh` script

```sh
# -- SET ENV VARS -- #
# bucket the webhook and jar file are uploaded to
BUCKET_NAME=

# secret to authenticate and (implicitly) authorize webhook update request
WEBHOOK_AUTH_SECRET=

# path to the jar file in the bucket
# ex: /app.jar (if file is in the root bucket directory)
BUCKET_JAR_PATH=/app.jar
# -- END ENV VARS -- #
```

2. call the deliver script (will write env var values to `webhook/setup.sh`)

```sh
$ bash deliver.sh

# outputs the command used in step 4.
```

3. SSH into your instance

4. run the command outputted by the deliver script to copy the webhook files

```sh
# run within your EC2 instance

# the command with your bucket name included will be output by the deliver script
# it will look like this:
$ aws s3 cp --recursive s3://BUCKET_NAME/webhook /etc/opt/zika/webhook
```

5. run the webhook setup script

```sh
# run within your EC2 instance
$ bash /etc/opt/zika/webook/setup.sh
```

6. add a rule for port `8008` (webhook port) in your EC2 instance security group.

```yaml
type: Custom TCP
port: 8008
source: your IP or SG of deployed Jenkins EC2 instance
```

7. start using the webhook!

From your local machine, Jenkins or another other pipeline tool just issue a request to `http://<public DNS name>:8008/update` to trigger the webhook.

**Don't forget to include the `Authorization` header with your webhook secret.**

Here is an example with `curl`:

```sh
$ curl http://<public DNS name>:8008/update -H "Authorization: <WEBHOOK_AUTH_SECRET>"
```

## Note

The webhook server only responds with the following status codes:

> the update script will restart the API (zika.service) using the new `app.jar`. But it has no guarantee that the API runs without failure after it is restarted. A dependable pipeline can assure its success once reaching this deployment step.

```yaml
200: update script called without failure (DOES NOT guarantee the API server ran without error)

403: request to /update with an invalid or missing Authorization header

404: requests to any path besides /update

500: update script failed. logs can be read with $ journalctl -fu webhook
```

If you want to automate behavior based on this response use the `-f` (fail) option with `curl`. If the response has a 200 status it will `exit 0` with no output to `stdout`. Otherwise a non-zero exit code is sent and a failure message is printed.

> the failure message can be suppressed by adding the `-s` (silent) option

Here is an example to use when scripting the request

```sh
# no stdout, fail with non-zero exit if response status is not 200
curl -sf localhost:8008/update -H 'Authorization: <WEBHOOK_AUTH_SECRET>'
```

<hr>

# Set Up EC2 Service Role

If you do not have an EC2 service role to grant your instance permission to use S3 follow the directions below

## Create IAM EC2 Service Role

> services > IAM > Roles > Create Role > AWS Service > EC2

> select create a new policy to add limited permissions to access the s3 bucket

## Create S3 Access Policy

> this will open a new tab for the policy creator

> **leave the role tab open**

- service: `S3`
- actions
  - read: `GetObject` for copying objects within the bucket
  - access level: `ListBucket` for recursive copying of bucket directories
- resources: `specific > add ARN`
  - bucket name: `<bucket name>`
  - object name: select any
  - expected: `arn:aws:s3:::<bucket name>/*`

> review and create policy

- name: `<name>-zika-data-s3-read-access`
- description: `read access to <name> zika s3 bucket objects`

> expected policy (JSON) should look like this

> **make sure you have the correct bucket name!**

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "VisualEditor0",
      "Effect": "Allow",
      "Action": ["s3:GetObject", "s3:ListBucket"],
      "Resource": ["arn:aws:s3:::<bucket-name>", "arn:aws:s3:::<bucket-name>/*"]
    }
  ]
}
```

## Attach Policy to Service Role

> return to role tab and hit refresh button for policies

> select filter policies > customer managed > select the created policy: `<name>-zika-data-s3-read-access`

> create the role

- role name: `<name>-zika-ec2-aws-access`
- description: `allow <name> ec2 instances with this role to access aws resources dictated by attached policies`

## Attach Service Role to EC2 Instance

> services > ec2 > instances > select instance

> actions > instance settings > attach / replace IAM role > select `<name>-zika-ec2-aws-access` > apply

You do not need to restart the instance but you may need to wait up to 30 seconds for the role to take affect.
